TARGET=review.pdf

all: $(TARGET)

%.pdf: %.tex %.sty
	latexmk -pdf $^
